/*
 * MODIFIED FROM sylvan_mtbdd.c
 *
 * Copyright 2011-2016 Formal Methods and Tools, University of Twente
 * Copyright 2016-2017 Tom van Dijk, Johannes Kepler University Linz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Extends the functionality of leq from sylvan_mtbdd.c to BDDs.
 */

#include "info_scce_addlib_sylvan_Sylvan.h"
#include "sylvan_leq.h"
#include <sylvan_int.h>

/**
 * For two BDDs a, b, return sylvan_true if all common assignments a(s) <= b(s), sylvan_false otherwise.
 */
TASK_2(BDD, sylvan_leq_rec, BDD, a, BDD, b)
{
    /* Check terminal cases */
    if (a == b) return sylvan_true;
    if (b == sylvan_true) return sylvan_true;
    if (a == sylvan_true) return sylvan_false;
    if (a == sylvan_false) return sylvan_true;

	if (sylvan_not(a) == b) return sylvan_false;

    /* Maybe perform garbage collection */
    sylvan_gc_test();

    /* Check cache */
    BDD result;
    if (cache_get3(CACHE_BDD_LEQ, a, b, 0, &result)) {
        return result;
    }

    bddnode_t na = sylvan_isconst(a) ? 0 : MTBDD_GETNODE(a);
    bddnode_t nb = sylvan_isconst(b) ? 0 : MTBDD_GETNODE(b);

    /* Get top variable */
	BDDVAR va = na ? bddnode_getvariable(na) : 0xffffffff;
	BDDVAR vb = nb ? bddnode_getvariable(nb) : 0xffffffff;
	BDDVAR level = va < vb ? va : vb;

	/* Get cofactors */
    BDD aLow, aHigh, bLow, bHigh;
	aLow  = va == level ? node_getlow(a, na)  : a;
	aHigh = va == level ? node_gethigh(a, na) : a;
	bLow  = vb == level ? node_getlow(b, nb)  : b;
	bHigh = vb == level ? node_gethigh(b, nb) : b;

	SPAWN(sylvan_leq_rec, aHigh, bHigh);
	result = CALL(sylvan_leq_rec, aLow, bLow);
	if (result != SYNC(sylvan_leq_rec)) result = sylvan_false;

    /* Store in cache */
	cache_put3(CACHE_BDD_LEQ, a, b, 0, result);

    return result;
}

TASK_IMPL_2(BDD, sylvan_leq, BDD, a, BDD, b)
{
    return CALL(sylvan_leq_rec, a, b);
}

JNIEXPORT jint JNICALL Java_info_scce_addlib_sylvan_Sylvan_native_1sylvan_1leq
  (JNIEnv *, jclass, jlong f, jlong g)
{
  return (jint)(sylvan_leq((BDD)f, (BDD)g) == sylvan_true);
}