/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.serializer;

import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;

/**
 * Handles the serialization and deserialization of XDDs.
 *
 * @param <E> Type of XDD
 */
public class XDDSerializer<E> extends DDSerializer<XDDManager<E>, XDD<E>> {

    @Override
    protected XDD<E> parseConstant(XDDManager<E> ddManager, String content) {
        E value = ddManager.parseElement(content);
        return ddManager.constant(value);
    }

    @Override
    protected String constantToString(XDD<E> dd) {
        return String.valueOf(dd.v());
    }

    @Override
    protected XDD<E> ithVar(XDDManager<E> ddManager, int i, XDD<E> t, XDD<E> e) {
        return ddManager.ithVar(i, t, e);
    }

    @Override
    protected XDD<E> namedVar(XDDManager<E> ddManager, String name, XDD<E> t, XDD<E> e) {
        return ddManager.namedVar(name, t, e);
    }

    @Override
    protected XDD<E> namedIthVar(XDDManager<E> ddManager, String name, int i, XDD<E> t, XDD<E> e) {
        return ddManager.namedIthVar(name, i, t, e);
    }
}
