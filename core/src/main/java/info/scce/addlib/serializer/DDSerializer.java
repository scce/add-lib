/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.serializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import info.scce.addlib.dd.DD;
import info.scce.addlib.dd.DDManager;
import info.scce.addlib.dd.DDManagerException;
import info.scce.addlib.traverser.PostorderTraverser;
import info.scce.addlib.util.IOUtils;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * General serialization of DDs.
 *
 * @param <M> DD manager
 * @param <D> Type of DD
 */
public abstract class DDSerializer<M extends DDManager<D, ?>, D extends DD<M, D>> {

    /* Serialisation */

    /**
     * Serializes a DD.
     *
     * @param f DD to serialize
     * @return Serialized DD
     */
    public String serialize(D f) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        serialize(pw, f);
        return sw.toString();
    }

    /**
     * Serializes a DD and writes the result to a given file.
     *
     * @param file File to write to
     * @param f    DD to serialize
     */
    public void serialize(File file, D f) throws IOException {
        try (PrintWriter pw = IOUtils.getBufferedPrintWriterUTF8(file)) {
            serialize(pw, f);
        }
    }

    /**
     * Serializes a DD and writes the result to the given OutputStream.
     *
     * @param out Steam to write to
     * @param f   DD to serialize
     */
    public void serialize(OutputStream out, D f) {
        try (PrintWriter pw = IOUtils.getOutputStreamPrintWriterUTF8(out)) {
            serialize(pw, f);
        }
    }

    /**
     * Handles the serialization behavior.
     *
     * @param pw Writer to use for the serialization
     * @param f  DD to serialize
     */
    protected void serialize(PrintWriter pw, D f) {
        for (D g : new PostorderTraverser<>(f)) {
            if (g.isConstant()) {
                pw.print(g.ptr() + ";");
                pw.print("constant" + ";");
                pw.print(escapeString(constantToString(g)) + ";");
                pw.println();
            } else {
                pw.print(g.ptr() + ";");
                pw.print("non-constant" + ";");
                pw.print(escapeString(g.readName()) + ";");
                pw.print(g.t().ptr() + ";");
                pw.print(g.e().ptr() + ";");
                pw.print(g.readPerm() + ";");
                pw.println();
            }
        }
        pw.println(f.ptr() + ";" + "root" + ";");
        pw.flush();
    }

    /* Deserialisation */

    /**
     * Deserializes a DD from a string.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param str        String with the serialized DD
     * @param ddProperty Property to use for deserialization of variables
     * @return Deserialized DD
     * @see DDProperty
     */
    public D deserialize(M ddManager, String str, DDProperty ddProperty) {
        try (StringReader sr = new StringReader(str); Scanner sc = new Scanner(sr)) {

            return deserialize(ddManager, sc, ddProperty);
        }
    }

    /**
     * Deserializes a DD from a file.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param file       File with the serialized DD
     * @param ddProperty Property to use for deserialization of variables
     * @return Deserialized DD
     * @see DDProperty
     */
    public D deserialize(M ddManager, File file, DDProperty ddProperty) throws FileNotFoundException {
        return deserialize(ddManager, file, ddProperty, "UTF-8");
    }

    /**
     * Deserializes a DD from a file with a given charset.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param file       File with the serialized DD
     * @param ddProperty Property to use for deserialization of variables
     * @param charset    Charset for the file
     * @return Deserialized DD
     * @see DDProperty
     */
    public D deserialize(M ddManager, File file, DDProperty ddProperty, String charset) throws FileNotFoundException {
        try (Scanner sc = new Scanner(file, charset)) {
            return deserialize(ddManager, sc, ddProperty);
        }
    }

    /**
     * Deserializes a DD from an InputStream.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param in         InputStream with the serialized DD
     * @param ddProperty Property to use for deserialization of variables
     * @return Deserialized DD
     * @see DDProperty
     */
    public D deserialize(M ddManager, InputStream in, DDProperty ddProperty) {
        return deserialize(ddManager, in, ddProperty, "UTF-8");
    }

    /**
     * Deserializes a DD from an input stream with a given charset.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param in         InputStream with the serialized DD
     * @param ddProperty Property to use for deserialization of variables
     * @param charset    Charset for the input stream
     * @return Deserialized DD
     * @see DDProperty
     */
    public D deserialize(M ddManager, InputStream in, DDProperty ddProperty, String charset) {
        Scanner sc = new Scanner(in, charset);
        return deserialize(ddManager, sc, ddProperty);
    }

    /**
     * Handles the deserialization behavior.
     *
     * @param ddManager  Manager for the deserialized DD
     * @param sc         Scanner to use for the deserialization
     * @param ddProperty Property to use for deserialization of variables
     */
    @SuppressWarnings("PMD.PrematureDeclaration")
    private D deserialize(M ddManager, Scanner sc, DDProperty ddProperty) {

        /* Build intermediate results */
        Map<Long, @NonNull D> cache = new HashMap<>();
        D result = null;

        while (result == null && sc.hasNextLine()) {

            /* Parse common part of the line */
            String line = sc.nextLine();
            String[] parts = line.split(";");
            long id = Long.parseLong(parts[0]);
            String type = parts[1];

            /* Treat cases separately */
            if ("root".equals(type)) {

                /* This is the root. We're done. */
                result = cache.get(id);
                if (result == null) {
                    throw new IllegalArgumentException("The given input to deserialize has an invalid format!");
                }
                result.ref();

            } else if ("constant".equals(type)) {

                /* Constant decision diagram defined by its value */
                String strValue = unescapeString(parts[2]);
                D f = parseConstant(ddManager, strValue);
                cache.put(id, f);

            } else if ("non-constant".equals(type)) {

                /* Non-constant decision diagram defined by its variable name and children */
                String varName = unescapeString(parts[2]);
                long idThen = Long.parseLong(parts[3]);
                long idElse = Long.parseLong(parts[4]);
                int varIndex = Integer.parseInt(parts[5]);

                /* Find children in cache (They have appeared earlier) */
                D t = cache.get(idThen);
                D e = cache.get(idElse);
                if (t == null || e == null) {
                    throw new IllegalArgumentException("The given input to deserialize has an invalid format!");
                }
                @Nullable D f = null;
                switch (ddProperty) {
                    case VARINDEX:
                        f = ithVar(ddManager, varIndex, t, e);
                        break;
                    case VARNAME:
                        f = namedVar(ddManager, varName, t, e);
                        break;
                    case VARNAMEANDVARINDEX:
                        try {
                            f = namedIthVar(ddManager, varName, varIndex, t, e);
                        } catch (DDManagerException ex) {
                            /* Release intermediate results */
                            for (D dd : cache.values()) {
                                dd.recursiveDeref();
                            }
                            throw ex;
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("DDProperty is invalid!");
                }
                cache.put(id, f);

            }
        }

        if (result == null) {
            throw new IllegalArgumentException("Could not parse a XDD with the given input!");
        }

        /* Release intermediate results */
        for (D f : cache.values()) {
            f.recursiveDeref();
        }

        return result;
    }

    /* String escaping */

    protected String escapeString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            /* Replace NL, CR, colon, and backslash with escaped two-char sequence */
            if (c == '\n') {
                sb.append("\\n");
            } else if (c == '\r') {
                sb.append("\\r");
            } else if (c == ';') {
                sb.append("\\c");
            } else if (c == '\\') {
                sb.append("\\b");
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    @SuppressWarnings("PMD.AvoidReassigningLoopVariables")
    protected String unescapeString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == '\\') {

                /* Escaped sequence detected: next char determines NL, CR, colon, or backslash */
                char c2 = str.charAt(++i);
                if (c2 == 'n') {
                    sb.append('\n');
                }
                if (c2 == 'r') {
                    sb.append('\r');
                }
                if (c2 == 'c') {
                    sb.append(';');
                }
                if (c2 == 'b') {
                    sb.append('\\');
                }

            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /* Wrapper methods */

    protected abstract D parseConstant(M ddManager, String content);

    protected abstract String constantToString(D dd);

    protected abstract D ithVar(M ddManager, int i, D t, D e);

    protected abstract D namedVar(M ddManager, String name, D t, D e);

    protected abstract D namedIthVar(M ddManager, String name, int i, D t, D e);
}
