/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.dd.add;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import info.scce.addlib.apply.DD_AOP_Fn;
import info.scce.addlib.apply.DD_MAOP_Fn;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.ringlikedd.example.ArithmeticDDManager;
import info.scce.addlib.util.Conversions;
import info.scce.addlib.utils.DDConversions;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Arithmetic decision diagrams for doubles. Although ADDs share the abbreviation with algebraic decision diagrams,
 * {@link XDD} provides the actual algebraic decision diagrams.
 */
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ADD extends RegularDD<ADDManager, ADD> {

    public ADD(long ptr, ADDManager ddManager) {
        super(ptr, ddManager);
    }

    /* CUDD wrapper */

    /**
     * Returns the value of the DD if the DD is a leaf. Otherwise, throws an exception.
     *
     * @return Value of leaf
     */
    public double v() {
        assertConstant();
        return ddManager.getBackend().v(ptr);
    }

    /**
     * If-then-else for ADDs. Only works with a 0-1 ADD.
     */
    public ADD ite(ADD t, ADD e) {
        assertEqualDDManager(t, e);
        long resultPtr = ddManager.getBackend().ite(ddManager().ptr(), ptr, t.ptr, e.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Checks if calling {@link #ite(ADD, ADD)} results in a constant ADD.
     * Only works with a 0-1 ADD.
     * <p>
     * Only available in CUDD.
     *
     * @return Constant ADD if result is constant, otherwise {@code null}.
     */
    public @Nullable ADD iteConstant(ADD t, ADD e) {
        assertEqualDDManager(t, e);
        long resultPtr = ddManager.getBackend().iteConstant(ddManager().ptr(), ptr, t.ptr, e.ptr);
        // 1 is the pointer of DD_NON_CONSTANT in CUDD
        if (resultPtr == 1) {
            return null;
        }
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Checks whether g is constant whenever the ADD is 1.
     * Only works with a 0-1 ADD.
     * Returns null if condition not fulfilled, otherwise the resulting ADD.
     * <p>
     * Only available in CUDD.
     */
    public @Nullable ADD evalConst(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().evalConst(ddManager().ptr(), ptr, g.ptr);
        if (resultPtr == 1) {
            return null;
        }
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Complement of the ADD. Leaves with 0 are flipped to 1 and all other values to 0.
     */
    public ADD cmpl() {
        long resultPtr = ddManager.getBackend().cmpl(ddManager().ptr(), ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Determines whether the ADD is less than or equal {@code g}.
     * For 0-1 ADDs {@link #evalConst(ADD)} might be more efficient.
     *
     * @return {@code true} if ADD is less than or equal {@code g}
     */
    public boolean leq(ADD g) {
        assertEqualDDManager(g);
        int result = ddManager.getBackend().leq(ddManager().ptr(), ptr, g.ptr);
        return result > 0;
    }

    /**
     * Substitutes variable with index {@code v} in ADD with {@code g}.
     *
     * @param v Index of variable to substitute
     */
    public ADD compose(ADD g, int v) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().compose(ddManager.ptr(), ptr, g.ptr, v);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Substitutes all variables in an ADD with a vector of ADDs.
     * Uses the indices of the variables for the substitution.
     */
    public ADD vectorCompose(ADD... vector) {
        assertEqualDDManager(vector);
        long resultPtr = ddManager.getBackend().vectorComposeADD(ddManager().ptr(), ptr, DDConversions.ptrs(vector));
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Arithmetic addition.
     */
    public ADD plus(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().plus(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Casts leaves to floats, applies arithmetic addition and casts back to double.
     */
    public ADD plusFloat(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().plusFloat(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Maps leaf to 1 if sigmoid(leaf) &gt; 0.5, otherwise 0.
     */
    public ADD sigmoidPrediction() {
        long resultPtr = ddManager.getBackend().sigmoidPrediction(ddManager().ptr(), ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }


    /**
     * Applies the following function to leaves:
     * Arithmetic multiplication.
     */
    public ADD times(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().times(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * 1 if ADD is larger than or equal {@code g}, otherwise 0.
     */
    public ADD threshold(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().threshold(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Sets ADD to g wherever g != 0.
     * <p>
     * Only available in CUDD.
     */
    public ADD setNZ(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().setNZ(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Arithmetic division. Requires g to be constant.
     * Returns a 0-ADD when dividing by 0.
     */
    public ADD divide(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().divide(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Arithmetic subtraction.
     */
    public ADD minus(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().minus(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Minimum of ADD and g.
     */
    public ADD minimum(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().minimum(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Maximum of ADD and g.
     */
    public ADD maximum(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().maximum(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Returns 1 if ADD is larger than g, otherwise 0.
     */
    public ADD oneZeroMaximum(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().oneZeroMaximum(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Returns positive infinity if ADD is equal to g, otherwise the minimum of f and g.
     * <p>
     * Only available in CUDD.
     */
    public ADD diff(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().diff(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Applies the following function to leaves:
     * Value of ADD when equal to g, otherwise background constant.
     * <p>
     * Only available in CUDD.
     */
    public ADD agreement(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().agreement(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Logical or for two 0-1 ADDs.
     */
    public ADD or(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().or(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Logical nand for two 0-1 ADDs.
     */
    public ADD nand(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().nand(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Logical nor for two 0-1 ADDs.
     */
    public ADD nor(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().nor(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Logical xor for two 0-1 ADDs.
     */
    public ADD xor(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().xor(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Logical xnor for two 0-1 ADDs.
     */
    public ADD xnor(ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().xnor(ddManager().ptr(), ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Natural logarithm of ADD.
     */
    public ADD log() {
        long resultPtr = ddManager.getBackend().log(ddManager().ptr(), ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /* Non-trivial operations */

    /**
     * Calls a binary function on the values of ADD and {@code g} and computes the result.
     */
    public ADD apply(final BinaryOperator<Double> op, ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager.getBackend().apply(ddManager().ptr(), new DD_AOP_Fn() {

            @Override
            public long apply(long ddManagerPtr, long f, long g) {
                if (Conversions.asBoolean(ddManager.getBackend().isConstant(f)) &&
                    Conversions.asBoolean(ddManager.getBackend().isConstant(g))) {
                    double left = ddManager.getBackend().v(f);
                    double right = ddManager.getBackend().v(g);
                    try {
                        double result = op.apply(left, right);
                        return ddManager.getBackend().constant(ddManagerPtr, result);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return ddManager.getBackend().invalid();
                    }
                }
                return ddManager.getBackend().invalid();
            }
        }, this.ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Calls a binary function on the ADD and {@code g} and computes the result.
     */
    public ADD apply2(final BinaryOperator<ADD> op, final ADD g) {
        assertEqualDDManager(g);
        long resultPtr = ddManager().getBackend().apply(ddManager().ptr(), new DD_AOP_Fn() {

            @Override
            public long apply(long ddManager, long fPtr, long gPtr) {
                ADD f = new ADD(fPtr, ADD.this.ddManager);
                ADD g = new ADD(gPtr, ADD.this.ddManager);
                try {
                    ADD result = op.apply(f, g);
                    return result == null ? ddManager().getBackend().invalid() : result.ptr;
                } catch (Exception e) {
                    e.printStackTrace();
                    return ddManager().getBackend().invalid();
                }
            }
        }, ptr, g.ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Calls a unary function on the values of the ADD and computes the result.
     */
    public ADD monadicApply(final UnaryOperator<Double> op) {
        long resultPtr = ddManager().getBackend().monadicApply(ddManager().ptr(), new DD_MAOP_Fn() {

            @Override
            public long apply(long ddManagerPtr, long f) {
                if (Conversions.asBoolean(ddManager.getBackend().isConstant(f))) {
                    double x = ddManager().getBackend().v(f);
                    try {
                        double result = op.apply(x);
                        return ddManager().getBackend().constant(ddManagerPtr, result);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return ddManager().getBackend().invalid();
                    }
                }
                return ddManager().getBackend().invalid();
            }
        }, ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /**
     * Calls a unary function on the ADD itself and computes the result.
     */
    public ADD monadicApply2(final UnaryOperator<ADD> op) {
        long resultPtr = ddManager().getBackend().monadicApply(ddManager().ptr(), new DD_MAOP_Fn() {

            @Override
            public long apply(long ddManager, long fPtr) {
                ADD f = new ADD(fPtr, ADD.this.ddManager);
                try {
                    ADD result = op.apply(f);
                    return result == null ? ddManager().getBackend().invalid() : result.ptr;
                } catch (Exception e) {
                    e.printStackTrace();
                    return ddManager().getBackend().invalid();
                }
            }
        }, ptr);
        return new ADD(resultPtr, ddManager).withRef();
    }

    /* Conversion to XDD */

    /**
     * Transforms the ADD to an XDD with double values.
     *
     * @param ddManagerTarget Manager for the resulting XDD
     * @return Transformed ADD
     */
    public XDD<Double> toXDD(ArithmeticDDManager ddManagerTarget) {
        HashMap<ADD, XDD<Double>> cache = new HashMap<>();
        XDD<Double> result = this.toXDDRecursive(ddManagerTarget, cache);

        /* Dereference intermediate results */
        cache.remove(this);
        for (XDD<Double> g : cache.values()) {
            g.recursiveDeref();
        }

        return result;
    }

    private XDD<Double> toXDDRecursive(ArithmeticDDManager ddManagerTarget, Map<ADD, XDD<Double>> cache) {
        XDD<Double> result = cache.get(this);
        if (result == null) {
            if (isConstant()) {
                result = ddManagerTarget.constant(v());
            } else {
                String name = readName();
                int idx = ddManagerTarget.varIdx(name);
                XDD<Double> t = t().toXDDRecursive(ddManagerTarget, cache);
                XDD<Double> e = e().toXDDRecursive(ddManagerTarget, cache);
                result = ddManagerTarget.ithVar(idx, t, e);
            }
            cache.put(this, result);
        }
        return result;
    }

    /* Required DD methods */

    @Override
    protected ADD thisCasted() {
        return this;
    }

    @Override
    public ADD t() {
        assertNonConstant();
        long resultPtr = ddManager.getBackend().t(ptr);
        return new ADD(resultPtr, ddManager);
    }

    @Override
    public ADD e() {
        assertNonConstant();
        long resultPtr = ddManager.getBackend().e(ptr);
        return new ADD(resultPtr, ddManager);
    }

    @Override
    public ADD eval(boolean... input) {
        long resultPtr = ddManager.getBackend().eval(ddManager().ptr(), ptr, Conversions.asInts(input));
        return new ADD(resultPtr, ddManager);
    }

    /* Other methods */

    @Override
    public String toString() {
        if (isConstant()) {
            return Double.toString(v());
        }
        return super.toString();
    }
}
