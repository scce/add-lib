/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.dd;

import info.scce.addlib.util.Conversions;
import org.checkerframework.checker.nullness.qual.Nullable;

/**
 * Provides basic functions for all kinds of DDs.
 *
 * @param <M> Manager for the DD
 * @param <D> Type of DD matching the type used for the manager M
 */
public abstract class DD<M extends DDManager<D, ?>, D extends DD<?, D>> {

    protected final long ptr;
    protected final M ddManager;

    /**
     * Default constructor for DDs.
     * Requires a pointer and a manager.
     *
     * @param ptr       Pointer to DD
     * @param ddManager Manager that stores the DD
     */
    public DD(long ptr, M ddManager) {
        this.ptr = ptr;
        this.ddManager = ddManager;
    }

    /* Common DD methods */

    /**
     * Returns the pointer of the DD. Depends on the backend.
     * Refer to the documentation for further information.
     *
     * @return pointer to DD
     */
    public long ptr() {
        return ptr;
    }

    /**
     * Returns the regular version of pointer.
     * Relevant for CUDD when using BDDs.
     *
     * @return the regular version of pointer
     */
    public long regularPtr() {
        return this.ddManager.backend.regularPtr(ptr);
    }

    public M ddManager() {
        return ddManager;
    }

    /**
     * Adds the reference to the DD in the C context by increasing the reference count.
     * Required for the persistence of the DD.
     */
    public D withRef() {
        ref();
        return thisCasted();
    }

    /* CUDD wrapper */

    /**
     * Increases the reference count of the DD in the manager and the backend.
     */
    public void ref() {
        this.ddManager.incRefCount(ptr);
        this.ddManager.backend.ref(ptr);
    }

    /**
     * Decreases the reference count of the DD to dereference the DD.
     * If the reference count is zero, then the DD is cleaned up by the garbage collection.
     * <p>
     * Throws an exception when dereferencing a DD with no remaining references.
     */
    public void recursiveDeref() {

        /* Throws an exception if ptr is unreferenced */
        this.ddManager.decRefCount(ptr);
        this.ddManager.backend.deref(ddManager.ptr(), ptr);
    }

    /**
     * Returns the index/position in the order of the variables of the DD.
     * Changes in the variables order are ignored by this method.
     *
     * @return position of variable in order
     * @see #readPerm()
     */
    public int readIndex() {
        assertNonConstant();
        return this.ddManager.backend.readIndex(ptr);
    }

    /**
     * Returns the index/position in the current order of the variables of the DD.
     * Use this method when reordering DDs.
     *
     * @return position of variable in current order
     * @see #readIndex()
     */
    public int readPerm() {
        assertNonConstant();
        return this.ddManager.backend.readPerm(ddManager().ptr(), readIndex());
    }

    /**
     * Returns the name of the variable in the root.
     *
     * @return name of variable
     */
    public String readName() {
        return ddManager().varName(readIndex());
    }

    /**
     * Checks whether the root is a terminal node.
     *
     * @return <code>true</code> if terminal node
     */
    public boolean isConstant() {
        return Conversions.asBoolean(this.ddManager.backend.isConstant(ptr));
    }

    /**
     * Returns the size of the DD from the root.
     *
     * @return size of DD
     */
    public long dagSize() {
        return this.ddManager.backend.dagSize(ptr);
    }

    /* Required DD methods */

    protected abstract D thisCasted();

    /**
     * Returns the then DD of the root.
     * If the root is constant, an exception is thrown by C.
     *
     * @return DD when taking the then child
     */
    public abstract D t();

    /**
     * Returns the else DD of the root.
     * If the root is constant, an exception is thrown by C.
     *
     * @return DD when taking the else child
     */
    public abstract D e();

    /**
     * Evaluate the DD based on a given assignment.
     *
     * @param input Assignment of variables
     * @return value of evaluation
     */
    public abstract D eval(boolean... input);

    /* Assertions */

    protected void assertEqualDDManager(DD<?, ?> dd) {
        if (!ddManager.equals(dd.ddManager())) {
            throw new DDManagerException(
                    getClass().getSimpleName() + " operands must share the same " + DDManager.class.getSimpleName());
        }
    }

    protected void assertEqualDDManager(DD<?, ?>... dds) {
        for (DD<?, ?> dd : dds) {
            assertEqualDDManager(dd);
        }
    }

    protected void assertConstant() {
        if (!isConstant()) {
            throw new DDException("Expected constant " + getClass().getSimpleName());
        }
    }

    protected void assertNonConstant() {
        if (isConstant()) {
            throw new DDException("Expected non-constant " + getClass().getSimpleName());
        }
    }

    /* Other methods */

    @Override
    public boolean equals(@Nullable Object otherObj) {
        if (otherObj instanceof DD<?, ?>) {
            DD<?, ?> other = (DD<?, ?>) otherObj;
            return ptr == other.ptr;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Long.hashCode(ptr);
    }

    @Override
    public String toString() {
        if (isConstant()) {
            return "?";
        }
        return readName();
    }
}
