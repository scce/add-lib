/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.dd;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import info.scce.addlib.backend.Backend;
import info.scce.addlib.util.Conversions;

/**
 * Manages the DDs in a shared structure. Includes the variable order of the DDs.
 * Most DDs are created by the DD manager, especially constant values and variables.
 * Also offers methods related to reordering for CUDD.
 *
 * @param <D> Type of DD
 * @param <B> Backend used for the manager, currently either CUDD or Sylvan.
 *            Refer to {@link Backend} or the documentation for further information.
 */
public abstract class DDManager<D extends DD<?, ?>, B extends Backend> {

    protected final long ptr;
    private final Map<String, Integer> knownVarNames;
    private final Map<Integer, String> idToVarName;
    protected B backend;
    private final Map<Long, Integer> refCounts;

    /**
     * Creates a DD manager with the given backend.
     *
     * @param backend Backend for DD manager
     */
    public DDManager(B backend) {
        this(backend.init());
        this.backend = backend;
    }

    private DDManager(long ptr) {
        this.ptr = ptr;
        this.knownVarNames = new HashMap<>();
        this.idToVarName = new HashMap<>();
        this.refCounts = new HashMap<>();
    }

    public long ptr() {
        return ptr;
    }

    /**
     * Releases all resources allocated by the DD manager.
     * <p>
     * <b>WARNING: Sylvan can only use one DD manager overall.
     * For Sylvan this method has to be called on all DD managers before C releases the memory!</b>
     */
    public void quit() {
        backend.quit(ptr);
    }

    /* Reordering methods */

    /**
     * Reorders the DDs in the DD manager based on a heuristic
     * that is triggered if the combined size is over <code>minsize</code>.
     * Reordering is only available for CUDD.
     *
     * @param heuristic Reordering method to use
     * @param minsize   Only reorder when equal or above this threshold
     * @return <code>true</code> if successfully reordered, otherwise <code>false</code>
     * @see DDReorderingType
     */
    public boolean reduceHeap(DDReorderingType heuristic, int minsize) {
        int reordered = backend.reorder(ptr, heuristic, minsize);
        return Conversions.asBoolean(reordered);
    }

    /**
     * Returns the number of non-zero reference counts in the DD manager.
     * Can be used to check whether there are any memory leaks caused by failing to dereference DDs.
     *
     * @return Number of non-zero reference counts in DD manager
     */
    public long checkZeroRef() {
        return backend.getNumRefs(ptr);
    }

    /**
     * Set a fixed variable order based on the given permutation.
     * Reordering is only available for CUDD.
     *
     * @param permutation Array of indices for permutation
     * @return <code>true</code> if reordering was succesful, otherwise <code>false</code>
     */
    public boolean setVariableOrder(int[] permutation) {
        return backend.setVariableOrder(ptr, permutation);
    }

    /**
     * Enables automatic reordering for the DD manager using a given reordering heuristic.
     * {@link #setNextReordering(int)} can be used to set the minimum threshold of nodes
     * where reordering should be triggered.
     * Reordering is only available for CUDD.
     *
     * @param heuristic Method to use for reordering
     */
    public void enableAutomaticReordering(DDReorderingType heuristic) {
        backend.enableAutomaticReordering(ptr, heuristic);
    }

    /**
     * Disables automatic reordering for the DD manager.
     */
    public void disableAutomaticReordering() {
        backend.disableAutomaticReordering(ptr);
    }

    /**
     * Sets the number of nodes required to trigger
     * automatic reordering for {@link #enableAutomaticReordering(DDReorderingType)}.
     * Reordering is only available for CUDD.
     *
     * @param count Threshold of number of nodes at which reordering is triggered
     */
    public void setNextReordering(int count) {
        backend.setNextReordering(ptr, count);
    }

    /**
     * Saves the reference to the pointer of the DD in Java by increasing its counter.
     *
     * @param ptr Pointer to DD to save
     */
    public void incRefCount(long ptr) {
        int ptrRefCount = refCounts.getOrDefault(ptr, 0);
        refCounts.put(ptr, ptrRefCount + 1);
    }

    /**
     * Decreases the counter for the number of references to the DD.
     * When the reference count reaches zero, the reference is deleted.
     *
     * @param ptr Pointer to DD to remove
     * @throws DDManagerException is thrown when the counter for the DD is zero.
     */
    public void decRefCount(long ptr) {
        Integer refs = refCounts.get(ptr);
        if (refs == null) {
            throw new DDManagerException("Cannot dereference unreferenced DD");
        }

        if (refs > 1) {
            refCounts.put(ptr, refs - 1);
        } else {
            refCounts.remove(ptr);
        }
    }

    /**
     * Returns the name of the variable with the given index from the permuted order.
     *
     * @param i Index of the variable
     * @return Name of the variable
     */
    public int readPerm(int i) {
        return backend.readPerm(ptr, i);
    }

    /* Variable name mapping */

    /**
     * Converts a variable name to an index to use for the variable and saves the mapping to the known variable names.
     * Returns the mapped index if the variable name has been used previously.
     *
     * @param varName Variable name
     * @return index for the variable name
     */
    public int varIdx(String varName) {
        Integer id = knownVarNames.get(varName);
        if (id == null) {
            id = idToVarName.size();
            knownVarNames.put(varName, id);
            idToVarName.put(id, varName);
        }
        return id;
    }

    /**
     * Generates a variable name from an index to use for the variable and saves the mapping to the known variable names.
     * Returns the mapped name if the variable index has been used previously.
     *
     * @param varIdx Index of the variable
     * @return name for the variable
     */
    public String varName(int varIdx) {
        String name = idToVarName.get(varIdx);
        if (name == null) {
            name = "x" + varIdx;
            idToVarName.put(varIdx, name);
            knownVarNames.put(name, varIdx);
        }

        return name;
    }

    /**
     * Creates a variable name from the variable index of the DD.
     * @param dd DD to read the index from
     */
    protected void createVariableName(DD<?, ?> dd) {
        int idx = dd.readIndex();
        varName(idx);
    }

    /**
     * Returns the mapping of the variable indices to the names.
     * The mapping contains all variables created during the lifetime of the DD manager.
     *
     * @return the mapping of the variable names to indices
     */
    public Map<String, Integer> knownVarNames() {
        return knownVarNames;
    }

    public B getBackend() {
        return backend;
    }

    /**
     * Combines the functionality {@link #varName(int)} and {@link #varIdx(String)}
     * and allows the creation of a variable with a given variable name and index.
     *
     * @param varName Variable name to use for the variable
     * @param varIdx  Variable index to use for the variable
     * @throws DDManagerException Thrown when an entry for <code>varName</code> or <code>varIdx</code> exists
     *                            and the output of the mapping is unequal to the given argument.
     */
    public void addVarName(String varName, int varIdx) {
        Integer mappedVarIdx = knownVarNames.get(varName);
        if (mappedVarIdx != null && !Objects.equals(mappedVarIdx, varIdx)) {
            throw new DDManagerException("Variable " + varName + " has already been mapped to another index");
        }

        String mappedVarName = idToVarName.get(varIdx);
        if (mappedVarName != null && !Objects.equals(mappedVarName, varName)) {
            throw new DDManagerException("Another variable has already been mapped to the index " + varIdx);
        }

        knownVarNames.put(varName, varIdx);
        idToVarName.put(varIdx, varName);
    }

    /* Required DDManager methods */

    /**
     * Creates a DD with the given variable name.
     * <br>
     * Undefined behavior when not all indices up to the highest are mapped
     * and the mapping with {@code name} does not exist yet.
     *
     * @param name Variable name for root variable
     * @return created DD
     */
    public abstract D namedVar(String name);

    /**
     * Creates a DD with the given variable index.
     * If the index has not been mapped yet,
     * it creates a mapping of the format {@code "x"+i}.
     * <br>
     * If you create variables out of order or leave out indices,
     * the behavior of the ADD-Lib might be undefined as it assumes
     * all indices up to the highest to be mapped!
     *
     * @param i Index for root variable
     * @return created DD
     */
    public abstract D ithVar(int i);

    /**
     * Parse a DD from the given String. Refer to the respective child classes for further details.
     *
     * @param str String to parse
     * @return parsed DD
     */
    public abstract D parse(String str);

}
