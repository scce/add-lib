/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.dd.add;

import info.scce.addlib.backend.ADDBackend;
import info.scce.addlib.backend.BackendProvider;
import info.scce.addlib.dd.DDManager;
import info.scce.addlib.parser.ADDLanguageLexer;
import info.scce.addlib.parser.ADDLanguageParser;
import info.scce.addlib.parser.ADDVisitor;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 * DD manager for {@link ADD}.
 */
public class ADDManager extends DDManager<ADD, ADDBackend> {

    /**
     * Creates an ADD manager with one of the available backends.
     * Use {@link #ADDManager(ADDBackend)} instead when aiming for more precise results
     * as there are no guarantees which backend is used with the getter.
     */
    public ADDManager() {
        this(BackendProvider.getADDBackend());
    }

    /**
     * Creates an ADD manager with the given backend.
     *
     * @param backend Backend to use for ADDManager
     */
    public ADDManager(ADDBackend backend) {
        super(backend);
    }

    /**
     * Reads the epsilon parameter of the DD manager used for the precision of floating point values.
     * The default value is 1.0E-12.
     * <p>
     * Only available in CUDD.
     *
     * @return epsilon parameter
     */
    public double readEpsilon() {
        return getBackend().readEpsilon(ptr);
    }

    /**
     * Sets the epsilon parameter of the DD manager used for the precision of floating point values.
     * The default value is 1.0E-12.
     * <p>
     * Only available in CUDD.
     *
     * @param epsilon New epsilon parameter
     */
    public void setEpsilon(double epsilon) {
        getBackend().setEpsilon(ptr, epsilon);
    }

    /**
     * Returns the background constant for the DD manager.
     * Refer to the CUDD documentation for more information.
     * The default value is 0.
     * <p>
     * Only available in CUDD.
     */
    public double readBackground() {
        long backgroundPtr = getBackend().readBackground(ptr);
        return getBackend().v(backgroundPtr);
    }

    /**
     * Sets the background constant for the DD manager.
     * Only changes the background constant when given a constant ADD.
     * Refer to the CUDD documentation for more information.
     * The default value is 0
     * <p>
     * Only available in CUDD.
     *
     * @param background ADD with the constant value
     */
    public void setBackground(ADD background) {
        if (background.isConstant()) {
            getBackend().setBackground(ptr, background.ptr());
        }
    }

    /* Construct primitive ADDs */

    /**
     * Returns an ADD with constant 1.
     */
    public ADD readOne() {
        long ddNodePtr = getBackend().readOne(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    /**
     * Returns an ADD with constant 0.
     */
    public ADD readZero() {
        long ddNodePtr = getBackend().readZero(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    /**
     * Returns an ADD with positive infinity.
     * <p>
     * Only available in CUDD.
     */
    public ADD readPlusInfinity() {
        long ddNodePtr = getBackend().readPlusInfinity(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    /**
     * Returns an ADD with negative infinity.
     * <p>
     * Only available in CUDD.
     */
    public ADD readMinusInfinity() {
        long ddNodePtr = getBackend().readMinusInfinity(ptr);
        return new ADD(ddNodePtr, this).withRef();
    }

    /**
     * Returns an ADD with a given constant value.
     */
    public ADD constant(double value) {
        long ddNodePtr = getBackend().constant(ptr, value);
        return new ADD(ddNodePtr, this).withRef();
    }

    @Override
    public ADD namedVar(String name) {
        int i = varIdx(name);
        return backendIthVar(i);
    }

    public ADD namedVar(String name, ADD t, ADD e) {
        return ithVar(varIdx(name), t, e);
    }

    public ADD namedIthVar(String name, int i) {
        addVarName(name, i);
        return ithVar(i);
    }

    public ADD namedIthVar(String name, int i, ADD t, ADD e) {
        addVarName(name, i);
        return ithVar(i, t, e);
    }

    @Override
    public ADD ithVar(int i) {
        varName(i);
        return backendIthVar(i);
    }

    public ADD ithVar(int i, ADD t, ADD e) {
        ADD ithVar = ithVar(i);
        ADD result = ithVar.ite(t, e);
        ithVar.recursiveDeref();
        return result;
    }

    private ADD backendIthVar(int i) {
        long ddNodePtr = getBackend().ithVar(ptr, i);
        return new ADD(ddNodePtr, this).withRef();
    }

    /**
     * Creates an ADD with a new variable at the largest existing index plus 1.
     * <p>
     * Only available in CUDD.
     *
     * @return created ADD
     */
    public ADD newVar() {
        long ddNodePtr = getBackend().newVar(ptr);
        ADD result = new ADD(ddNodePtr, this).withRef();
        createVariableName(result);
        return result;
    }

    /**
     * Creates an ADD with a new variable at the given level and index of the largest existing index plus 1.
     * <p>
     * Only available in CUDD.
     *
     * @param level position of the variable
     * @return created ADD
     */
    public ADD newVarAtLevel(int level) {
        long ddNodePtr = getBackend().newVarAtLevel(ptr, level);
        ADD result = new ADD(ddNodePtr, this).withRef();
        createVariableName(result);
        return result;
    }

    @Override
    public ADD parse(String str) {
        ANTLRInputStream inputStream = new ANTLRInputStream(str);
        ADDLanguageLexer lexer = new ADDLanguageLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ADDLanguageParser parser = new ADDLanguageParser(tokens);
        ADDVisitor ast = new ADDVisitor(this);
        return ast.visit(parser.expr());
    }
}
