/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.serializer;

import info.scce.addlib.backend.ADDBackend;
import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.add.ADD;
import info.scce.addlib.dd.add.ADDManager;
import info.scce.addlib.utils.BackendProvider;
import org.testng.annotations.Test;
import static info.scce.addlib.serializer.DDProperty.VARNAME;
import static org.testng.Assert.assertEquals;

public class ADDSerializerTest extends DDManagerTest {

    @Test(dataProviderClass = BackendProvider.class, dataProvider = "cuddADDBackend")
    public void testSerializationWithReordering(ADDBackend addBackend) {

        /* Build ADD */
        ADDManager addManager = new ADDManager(addBackend);
        ADD x0 = addManager.namedVar("a");
        ADD x1 = addManager.namedVar("b");
        ADD result = x0.plus(x1);

        /* Release space for reordering to take place */
        x0.recursiveDeref();
        x1.recursiveDeref();

        /* Serialize ADD */
        ADDSerializer addSerializer = new ADDSerializer();
        String test = addSerializer.serialize(result);

        /* Deserialize and release space */
        ADDManager addManagerCopy = new ADDManager(addBackend);
        ADD deserialized = addSerializer.deserialize(addManager, test, VARNAME);

        /* Assert that the deserialized DD is the same */
        assertEquals(deserialized.readName(), result.readName());
        assertEquals(deserialized.t().readName(), result.t().readName());
        assertEquals(deserialized.e().readName(), result.e().readName());
        assertEquals(deserialized.t().t().v(), result.t().t().v());
        assertEquals(deserialized.t().e().v(), result.t().e().v());
        assertEquals(deserialized.e().t().v(), result.e().t().v());
        assertEquals(deserialized.e().e().v(), result.e().e().v());

        result.recursiveDeref();
        deserialized.recursiveDeref();

        assertRefCountZeroAndQuit(addManager);
        assertRefCountZeroAndQuit(addManagerCopy);
    }
}
