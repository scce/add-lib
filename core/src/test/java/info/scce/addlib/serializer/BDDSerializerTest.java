/* Copyright (c) 2017-2023, TU Dortmund University
 * This file is part of ADD-Lib, https://add-lib.scce.info/.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 * disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * Neither the name of the TU Dortmund University nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package info.scce.addlib.serializer;

import info.scce.addlib.backend.BDDBackend;
import info.scce.addlib.dd.DDManagerTest;
import info.scce.addlib.dd.DDReorderingType;
import info.scce.addlib.dd.bdd.BDD;
import info.scce.addlib.dd.bdd.BDDManager;
import info.scce.addlib.utils.BackendProvider;
import org.testng.annotations.Test;
import static info.scce.addlib.serializer.DDProperty.VARNAME;
import static org.testng.Assert.assertEquals;

public class BDDSerializerTest extends DDManagerTest {

    @Test(dataProviderClass = BackendProvider.class, dataProvider = "cuddBDDBackend")
    public void testSerializationWithReordering(BDDBackend bddBackend) {

        /*
         *       result
         *          \
         *           a
         *          /  \
         *         :    \
         *         |    |
         *         :    |
         * x1x3 - b     b - x1A2
         *      : |    /|
         *     /  |   : |
         *     :  d  :  c
         *     |  : \: /|
         *     \  : / \ |
         *        0     1
         *
         */

        /* Build BDD */
        BDDManager bddManager = new BDDManager(bddBackend);
        BDD x0 = bddManager.namedVar("a");
        BDD x1 = bddManager.namedVar("b");
        BDD x2 = bddManager.namedVar("c");
        BDD x3 = bddManager.namedVar("d");
        BDD x0Not = x0.not();
        BDD x1A2 = x1.and(x2);
        BDD conj1 = x0.and(x1A2);
        BDD x1A3 = x1.and(x3);
        BDD conj2 = x0Not.and(x1A3);
        BDD result = conj1.or(conj2);

        /* Release space for reordering to take place */
        x0.recursiveDeref();
        x1.recursiveDeref();
        x2.recursiveDeref();
        x3.recursiveDeref();
        x0Not.recursiveDeref();
        x1A2.recursiveDeref();
        conj1.recursiveDeref();
        x1A3.recursiveDeref();
        conj2.recursiveDeref();

        /* after reordering:
         *
         *       result
         *          \
         *           a
         *          /  \
         *         :    \
         *         d    c
         *         :\  :
         *         | \/
         *         : :\
         *         |/  b
         *         :  : \
         *         | /  |
         *         0    1
         *
         */

        /* Reorder and serialize BDD */
        bddManager.reduceHeap(DDReorderingType.EXACT, 0);
        long sizeExpected = result.dagSize();
        BDDSerializer bddSerializer = new BDDSerializer();
        String test = bddSerializer.serialize(result);

        /* Deserialize and release space */
        BDDManager bddManagerCopy = new BDDManager(bddBackend);
        BDD deserialized = bddSerializer.deserialize(bddManager, test, VARNAME);
        long sizeActual = deserialized.dagSize();

        result.recursiveDeref();
        deserialized.recursiveDeref();

        /* Assert that the size of the BDD before and after deserialization is equal */
        assertEquals(sizeActual, sizeExpected);
        assertRefCountZeroAndQuit(bddManager);
        assertRefCountZeroAndQuit(bddManagerCopy);
    }
}
