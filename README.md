# ADD-Lib

The ADD-Lib is a java library for various types of decision diagrams.
The library comprises Binary Decision Diagrams (BDDs), Zero-supressed Decision Diagrams (ZDDs), Algebraic Decision Diagrams (ADDs), and supports the seamless adaptation to custom algebraic structures.
Under the hood, the ADD-Lib uses the well-known decision diagram libraries [CUDD](https://github.com/dcreager/cudd) and [Sylvan](https://github.com/trolando/sylvan).

Besides the creation and manipulation of decision diagrams, the ADD-Lib provides a variety of features such as:
- [Code generation](https://gitlab.com/scce/add-lib/-/wikis/Use%20Cases/Serialization): The ADD-Lib offers code generators that translate a decision diagram into a program in a target language (e.g., C, C++, Java, Python, etc.).
- [Visualization](https://gitlab.com/scce/add-lib/-/wikis/Use%20Cases/Visualization): A decision diagram can be visualized as a graph at runtime. We also offer code generators that export a decision diagram as a graph in GraphViz's dot format or as ASCII art (see [visualization generators](https://gitlab.com/scce/add-lib/-/wikis/Use%20Cases/Code%20Generators#visualization-generators)).
- [Serialization](https://gitlab.com/scce/add-lib/-/wikis/Use%20Cases/Serialization): Decision diagrams in the ADD-Lib can be serialized and subsequently deserialized.

## Usage
To use the latest release of the ADD-Lib in your Maven project, simply include the following dependency in your `pom.xml`.

```xml
<dependencies>
    ...
    <dependency>
        <groupId>info.scce</groupId>
        <artifactId>addlib</artifactId>
        <version>3.0.0</version>
        <packaging>pom</packaging>
    </dependency>
</dependencies>
```

For details on how to use the ADD-Lib, take a look at the wiki: https://gitlab.com/scce/add-lib/-/wikis/home
